package com.company;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import javax.validation.constraints.Max;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.math.IntMath.pow;
import static java.lang.Math.max;

public class Bot extends TelegramLongPollingBot {

    List<Integer> members = new ArrayList<Integer>();

    @Override
    public String getBotToken() {
        return "749000631:AAGyUUeZnl-VsO4yXwth3BpA-o0xbk8ExEE";
    }

    @Override
    public String getBotUsername() {
        return "RomaPetroBot";
    }

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        String text = message.getText();
        System.out.println(text);

        String cmd = text.split(" ")[0];

        Long chatId = update.getMessage().getChatId();

        String send = "ᅠ  ";

        if (text.equals("/runbot")) {
            send = runbot();
        } else if (text.equals("/stop")) {
            send = stop();
        } else if (text.equals("/start")) {
            send = "commands:\n/runbot\n/stop\n/chatID ";
        } else if (text.equals("/chatID")){
            send = "" + chatId ;
        }  else if (cmd.equals("/member")) {
            String number = text.split(" ")[1];
            send = number;
        }  else if (cmd.equals("/forsum")) {
            String te = text.split(" ")[1];
            int tec = Integer.parseInt(te);
            int awancer = 0;
            for (int start = 1; start <= tec ; start = start + 1){
                awancer = awancer + start ;

            }
            send =  awancer + "";

        } else  if (cmd.equals("/forfac")){
            String te = text.split(" ")[1];
            int tec = Integer.parseInt(te);
            int awer = 1 ;
            for (int start = 1; start <= tec ; start = start + 1){
                awer = awer * start;
            }
            send = "" + awer;

        } else if (cmd.equals("/twodigit")) {
            int two = 0;
            while (two <= 96){
                two = two + 2;
               String te = "" + two;
                sendMessage(chatId,te);
            }
        } else if (cmd.equals("/nod")) {
         String sp1 = text.split(" ")[1];
         String sp2 = text.split(" ")[2];
         int first = Integer.parseInt(sp1);
         int second = Integer.parseInt(sp2);
         int ned = first * second;
         int nod = ned;
         while (ned != 1) {
             ned = ned - 1;
             if (first % ned == 0 && second % ned == 0  ) {
                 nod = ned;
                 break;
             }
         }
         String noda = "" + nod;
         send = noda;
        } else if (cmd.equals("/ned")) {
            String sp1 = text.split(" ")[1];
            String sp2 = text.split(" ")[2];
            int first = Integer.parseInt(sp1);
            int second = Integer.parseInt(sp2);
            int ned = first * second;
            int max = max(first, second);
            int nod = ned;
            while (ned != max) {
                ned = ned - 1;
                if (ned % first == 0 && ned % second == 0) {
                    nod = ned;
                }
            }
            String noda = "" + nod;
            send = noda;


        }

         if (cmd.equals("/mem")) {
           Integer num = 0;
            for (int i = 0; i < members.size(); i++) {
                sendMessage(chatId,i + " "   +  members.get(i));
                num = num + members.get(i);
            }
             sendMessage(chatId,"sum of numbers = " + num);
         } else if (cmd.equals("/add")) {
             String sp1 = text.split(" ")[1];
             Integer sp2 = Integer.parseInt(sp1);
             members.add(sp2);
         } else if (cmd.equals("/rem")) {
             String sp1 = text.split(" ")[1];
             Integer sp2 = Integer.parseInt(sp1);
             Integer obj = members.get(sp2);
             members.remove(obj);
         } else if (cmd.equals("/rad")) {
             int big = 0;
             int small = 0;
             for (int i = 0;members.size() > i;i++) {
                 Integer c = members.get(i);
                 if ( c > big ) {
                     big = c;
                 } if (c < small) {
                     small = c;
                 }
             }
             String b = "меньшее " + small + "\nбольшее " + big ;
             sendMessage(chatId, b);
         }


         if (cmd.equals("/no/2")) {
             no2(chatId);
         } else if (cmd.equals("/k")) {
             testKeyboard(chatId);
         }



        sendMessage(chatId, send);

    }

    String runbot() {
        String text = "bot is ready";
        return text;

    }
    String stop() {
        String text = "bot go offline";
        return text;
    }

    void sendMessage(long chatId, String text) {
        SendMessage message = new SendMessage()
                .setChatId(chatId)
                .setText(text);
        try {
            sendApiMethod(message);
        } catch (TelegramApiException e){
            e.printStackTrace();
        }
    }



    void part1(long chatID) {
        String[] names = {"Andrey","Alexey"};
        System.out.println(names[0] + "\n" + names[1]);
        sendMessage(chatID,names[0] + names[1]);

    }


    void no2(long chatId) {
        Integer i = 1;
        List<Integer> num = new ArrayList<Integer>();
        while (num.size() < 50) {
            num.add(i);
            i = i + 2;
        }
        sendMessage(chatId,"" + num);

    }

    void testKeyboard(Long chatiId) {
        ReplyKeyboardMarkup keyboard = getTestKeyboard();
        sendKeyboard(chatiId, "Hello", keyboard);
    }


    ReplyKeyboardMarkup getTestKeyboard() {
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup();
        keyboard.setSelective(true);
        keyboard.setResizeKeyboard(false);
        keyboard.setOneTimeKeyboard(true);

        List<KeyboardRow> array = new ArrayList<KeyboardRow>();


        KeyboardRow button = new KeyboardRow();
        button.add("/mem");
        array.add(button);

        KeyboardRow buton = new KeyboardRow();
        buton.add("/rad");
        array.add(buton);


        KeyboardRow rem = new KeyboardRow();
        rem.add(" ᅠ ");
        array.add(rem);

        keyboard.setKeyboard(array);
        return keyboard;
    }

    void sendKeyboard(Long chatId, String text, ReplyKeyboardMarkup keyboard) {
        SendMessage message = new SendMessage();
        message.enableMarkdown(true);
        message.setChatId(chatId);
        message.setText(text);
        message.setReplyMarkup(keyboard);
        try {
            sendApiMethod(message);
        } catch (TelegramApiException e){
            e.printStackTrace();
        }
    }
}







