package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Database {

    public static Map<Long,List< String>> logs = new HashMap<Long, List<String>>();
    public  static Map<Long, Boolean> adminMod = new HashMap<Long, Boolean>();
    public static String[] words = new String[]{"арбуз","абзац","аэропорт", "баклажан","виноград","град","дурак",
            "ёлка","енот","железо","зелень","иголка","Йод","колючка","лимон","мандарин",
            "ножницы","опера","персик","река", "снег","туфля","уж","финики","хлор",
            "цинк","человек","щука","швабра","эскимо","юла","ядро"};

    public static void test() {
        Writer writer = null;

        try {
            writer = new BufferedWriter(
                        new OutputStreamWriter(
                            new FileOutputStream("test.txt"), "utf-8"
                        )
            );
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
    }

    public static List<String> getDatabaseFromFile() {
        List<String> words = new ArrayList<String>();

        try {
            BufferedReader reader = new BufferedReader(
                    new FileReader(
                            new File("word_rus.txt")
                    )
            );

            String line = reader.readLine();
            while (line != null) {
                words.add(line);
                line = reader.readLine();
            }

            reader.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }


        return  words;
    }
}
